<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register'=>false]);

Route::get('/home', 'HomeController@index')->name('home');
//User Section
Route::get('/user-registration',[
    'uses' => 'UserRegistrationController@showRegistrationForm',
    'as' => 'user-registration'
])->middleware('auth');

Route::post('/user-registration',[
    'uses' => 'UserRegistrationController@userSave',
    'as' => 'userSave'
])->middleware('auth');

Route::get('/user-list',[
    'uses' => 'UserRegistrationController@userList',
    'as' => 'userList'
])->middleware('auth');

Route::get('/user-profile/{userId}',[
    'uses' => 'UserRegistrationController@userProfile',
    'as' => 'user-profile'
]);
Route::get('/change-user-info/{userId}',[
    'uses' => 'UserRegistrationController@changUserInfo',
    'as' => 'change-user-info'
]);
Route::post('/user-info-update',[
    'uses' => 'UserRegistrationController@userInfoUpdate',
    'as' => 'user-info-update'
]);
Route::get('/change-user-photo/{userId}',[
    'uses' => 'UserRegistrationController@changeUserPhoto',
    'as' => 'change-user-photo'
]);
Route::post('/update-user-photo',[
    'uses' => 'UserRegistrationController@updateUserPhoto',
    'as' => 'update-user-photo'
]);
Route::get('/change-user-password/{userId}',[
    'uses' => 'UserRegistrationController@changeUserPassword',
    'as' => 'change-user-password'
]);
Route::post('/update-user-password',[
    'uses' => 'UserRegistrationController@updateUserPassword',
    'as' => 'update-user-password'
]);
//User Section

//HomePageRoute
Route::get('/add-header-footer',[
    'uses' => 'HomePageController@addHeaderFooter',
    'as' => 'add-header-footer'
]);
Route::post('/add-header-footer',[
    'uses' => 'HomePageController@headerFooterSave',
    'as' => 'header-footer-save' 
]);
Route::get('/manage-header-footer/{id}',[
    'uses' => 'HomePageController@manageHeaderFooter',
    'as' => 'manage-header-footer' 
]);
Route::post('/update-manage-header-footer',[
    'uses' => 'HomePageController@updateManageHeaderFooter',
    'as' => 'update-manage-header-footer' 
]);

//Slide Section
Route::get('/add-slider',[
    'uses' => 'SlideController@addSlide',
    'as'   => 'add-slider'
]);
Route::post('/upload-slider',[
    'uses' => 'SlideController@uploadSlider',
    'as'   => 'upload-slider'
]);
Route::get('/manage-slider',[
    'uses' => 'SlideController@manageSlider',
    'as'   => 'manage-slider'
]);
Route::get('/slide-edit/{id}',[
    'uses' => 'SlideController@slideEdit',
    'as'   => 'slide-edit'
]);
Route::post('/update-slider',[
    'uses' => 'SlideController@updateSlider',
    'as'   => 'update-slider'
]);
Route::get('/slide-delete/{id}',[
    'uses' => 'SlideController@slideDelete',
    'as'   => 'slide-delete'
]);
Route::get('/unpublish-slide/{id}',[
    'uses' => 'SlideController@unpublishSlide',
    'as'   => 'unpublish-slide'
]);
Route::get('/publish-slide/{id}',[
    'uses' => 'SlideController@publishSlide',
    'as'   => 'publish-slide'
]);
Route::get('/gallery',[
    'uses' => 'SlideController@gallery',
    'as'   => 'gallery'
]);
//Slide Section

//School Section
Route::get('/school/add',[
    'uses' => 'SchoolManagementController@addSchoolForm',
    'as' => 'add-school'
]);
Route::post('/school/add',[
    'uses' => 'SchoolManagementController@saveSchool',
    'as' => 'save-school'
]);

Route::get('/school/manage',[
    'uses' => 'SchoolManagementController@manageSchool',
    'as' => 'manage-school'
]);

Route::get('/school/edit/{id}',[
    'uses' => 'SchoolManagementController@editSchool',
    'as' => 'edit-school'
]);
Route::get('/school/delete/{id}',[
    'uses' => 'SchoolManagementController@deleteSchool',
    'as' => 'delete-school'
]);
Route::post('/school/edit/',[
    'uses' => 'SchoolManagementController@updateSchool',
    'as' => 'update-school'
]);

Route::get('/school/unpublished/{id}',[
    'uses' => 'SchoolManagementController@unpublishedSchool',
    'as' => 'unpublished-school'
]);
Route::get('/school/published/{id}',[
    'uses' => 'SchoolManagementController@publishedSchool',
    'as' => 'published-school'
]);
//School Section

//Class Section

Route::get('/class/add',[
    'uses' => 'ClassManagementController@addClass',
    'as' => 'add-class'
]);
Route::post('/class/add',[
    'uses' => 'ClassManagementController@saveClass',
    'as' => 'save-class'
]);

Route::get('/class/manage',[
    'uses' => 'ClassManagementController@manageClass',
    'as' => 'manage-class'
]);

Route::get('/class/edit/{id}',[
    'uses' => 'ClassManagementController@editClass',
    'as' => 'edit-class'
]);

Route::post('/class/edit/',[
    'uses' => 'ClassManagementController@updateClass',
    'as' => 'update-class'
]);

Route::get('/class/delete/{id}',[
    'uses' => 'ClassManagementController@deleteClass',
    'as' => 'delete-class'
]);


Route::get('/class/unpublished/{id}',[
    'uses' => 'ClassManagementController@unpublishedClass',
    'as' => 'unpublished-class'
]);
Route::get('/class/published/{id}',[
    'uses' => 'ClassManagementController@publishedClass',
    'as' => 'published-class'
]);
//Class Section
//Class Section
Route::get('/batch/add',[
    'uses' => 'BatchManagementController@addBatch',
    'as' => 'add-batch'
]);
Route::post('/batch/add',[
    'uses' => 'BatchManagementController@saveBatch',
    'as' => 'save-batch'
]);

Route::get('/batch/manage',[
    'uses' => 'BatchManagementController@manageBatch',
    'as' => 'manage-batch'
]);
Route::get('/batch/batch-list-by-ajax',[
    'uses' => 'BatchManagementController@batchListByAjax',
    'as' => 'batch-list-by-ajax'
]);
Route::get('/batch/unpublished',[
    'uses' => 'BatchManagementController@batchUnpublished',
    'as' => 'batch-unpublished'
]);
Route::get('/batch/published',[
    'uses' => 'BatchManagementController@batchPublished',
    'as' => 'batch-published'
]);
Route::get('/batch/delete',[
    'uses' => 'BatchManagementController@batchDelete',
    'as' => 'batch-delete'
]);
Route::get('/batch/edit/{id}',[
    'uses' => 'BatchManagementController@batchEdit',
    'as' => 'edit-batch'
]);
Route::post('/batch/edit',[
    'uses' => 'BatchManagementController@updateBatch',
    'as' => 'update-batch'
]);
 
//Class Section

//Student Type 
Route::get('/student-type/index',[
    'uses' => 'StudentTypeController@studentTypeIndex',
    'as' => 'student-type-index'
]);

Route::post('/student-type/index',[
    'uses' => 'StudentTypeController@saveStudentType',
    'as' => 'save-student-type'
]);

Route::get('/student-type/student-type-list',[
    'uses' => 'StudentTypeController@studentTypeList',
    'as' => 'student-type-list'
]);
Route::get('/student-type/unpublish',[
    'uses' => 'StudentTypeController@studentTypeUnpublish',
    'as' => 'student-type-unpublish'
]);
Route::get('/student-type/publish',[
    'uses' => 'StudentTypeController@studentTypePublish',
    'as' => 'student-type-publish'
]);
Route::post('/student-type/update',[
    'uses' => 'StudentTypeController@updateStudentType',
    'as' => 'update-student-type'
]);
Route::get('/student-type/delete',[
    'uses' => 'StudentTypeController@studentTypeDelete',
    'as' => 'student-type-delete'
]);
//Student Type