<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Http\Request;
use Image;

class UserRegistrationController extends Controller
{
    //
    public function showRegistrationForm(){

        
        if(Auth::user()->role=='Admin'){
            return view('admin.users.registration-form');
        }else{
            return redirect('/home');
        }
    }

    public function userSave(Request $request){
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $users = User::all();
        return view('admin.users.user-list',compact('users')); 
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'role' => ['required'],
            'name' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'string', 'max:13','min:13'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'role' => $data['role'],
            'name' => $data['name'],
            'mobile' => $data['mobile'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function userList(){
        $users = User::all();
        return view('admin.users.user-list',compact('users')); 
    }

    public function userProfile($userId){
        $user = User::find($userId);
        // return $user;

        return view('admin.users.user-profile',compact('user'));
    }

    public function changUserInfo($userId){

        $user = User::find($userId);
        return view('admin.users.change-user-info',compact('user')); 
    }

    public function userInfoUpdate(Request $request){ 
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'mobile' => 'required|string|max:13|min:13',
            'email' => 'required|string|email|max:255'
        ]);
        $user = User::find($request->user_id);

        $user->name = $request->name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->save();

        return redirect("/user-profile/$request->user_id")->with('message','Operation Succeesfull');
    }

    public function changeUserPhoto($userId){

        $user = User::find($userId);

        return view('admin.users.change-user-photo',compact('user'));

    }
    public function updateUserPhoto(Request $request){
        $user = User::find($request->user_id);
        $file = $request->file('avatar');
        $imageName = $file->getClientOriginalName();
        $directory = 'admin/assets/avatar/';
        $imageUrl = $directory.$imageName;
        // $file->move($directory,$imageUrl);
        Image::make($file)->resize(300,300)->save($imageUrl);

        $user->avatar = $imageUrl;
        $user->save();
        return redirect("/user-profile/$request->user_id")->with('message','Upload Succeesfull');
    }

    public function changeUserPassword($id){
        $user = User::find($id);
        return view('admin.users.change-user-password',compact('user'));
    }

    public function updateUserPassword(Request $request){
        $this->validate($request,[
            'new_password' => 'required|string|min:8',
        ]);
        $oldPassword = $request->password;
        $user = User::find($request->user_id); 
        if (Hash::check($oldPassword, $user->password)) { 
            $user->password = Hash::make($request->new_password);
            $user->save();
            return redirect("/user-profile/$request->user_id")->with('message','Password Change Succeesfull');
        }else{
            return back()->with('error','Password does not matched');
        }
    }
}
