<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentType;
use App\ClassName;
use DB;
class StudentTypeController extends Controller
{
    //
    public function studentTypeIndex(){
        $studentTypes = DB::table('student_types')
                        ->join('class_names','student_types.class_id','=','class_names.id')
                        ->select('student_types.*','class_names.class_name')
                        ->get();
        $classes = ClassName::all();                
        return view('admin.settings.student-type.list',compact('studentTypes','classes'));
    }

    public function saveStudentType(Request $request){
        $this->validate($request,[
            'class_id' => 'required',
            'student_type' => 'required|string', 
        ]);

        if($request->ajax()){
            $studentType = new StudentType();
            $studentType->class_id = $request->class_id;
            $studentType->student_type = $request->student_type;
            $studentType->status = 1;
            $studentType->save(); 
        }
    }
    //😎Student Types Ajax Response😎
    public function studentTypeList(){
        $studentTypes = DB::table('student_types')
        ->join('class_names','student_types.class_id','=','class_names.id')
        ->select('student_types.*','class_names.class_name')
        ->get(); 

        return view('admin.settings.student-type.ajax-list',compact('studentTypes'));
    }

    //😊   student Type Unpublish 😊
    public function studentTypeUnpublish(Request $request){
        $studentType = StudentType::find($request->type_id);
        $studentType->status = 2;
        $studentType->save();

        $studentTypes = DB::table('student_types')
        ->join('class_names','student_types.class_id','=','class_names.id')
        ->select('student_types.*','class_names.class_name')
        ->get(); 
        return view('admin.settings.student-type.ajax-list',compact('studentTypes'));
    }

    //😊   student Type publish 😊
    public function studentTypePublish(Request $request){
        $studentType = StudentType::find($request->type_id);
        $studentType->status = 1;
        $studentType->save();

        $studentTypes = DB::table('student_types')
        ->join('class_names','student_types.class_id','=','class_names.id')
        ->select('student_types.*','class_names.class_name')
        ->get(); 
        return view('admin.settings.student-type.ajax-list',compact('studentTypes'));
    }

    //Update Student Type
    public function updateStudentType(Request $request){
        $this->validate($request,[ 
            'student_type' => 'required|string', 
        ]);
        $studentType = StudentType::find($request->type_id);
        $studentType->student_type = $request->student_type;
        $studentType->save();
        $studentTypes = DB::table('student_types')
        ->join('class_names','student_types.class_id','=','class_names.id')
        ->select('student_types.*','class_names.class_name')
        ->get();
    $classes = ClassName::all();                
    return view('admin.settings.student-type.ajax-list',compact('studentTypes','classes'));
    }

     //😊   student Type Delete 😊
     public function studentTypeDelete(Request $request){
        $studentType = StudentType::find($request->type_id); 
        $studentType->delete();
        $studentTypes = DB::table('student_types')
        ->join('class_names','student_types.class_id','=','class_names.id')
        ->select('student_types.*','class_names.class_name')
        // ->where('student_types.status','!=',3)
        ->get(); 
        return view('admin.settings.student-type.ajax-list',compact('studentTypes'));
    }
}
