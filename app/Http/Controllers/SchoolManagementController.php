<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;

class SchoolManagementController extends Controller
{
    // 
    public function addSchoolForm(){

        return view('admin.settings.school.add');
    }

    public function saveSchool(Request $request){
        $school = new School();
        $school->school_name = $request->school_name;
        $school->status = 1;
        $school->save();

        return back()->with('message','School Added');
    }

    //School Manage Or School List

    public function manageSchool(){
        $schools = School::all();
        return view('admin.settings.school.manage',compact('schools'));
    }

    //Unpublished School 

    public function unpublishedSchool($id){
        $school = School::find($id);

        $school->status = 2;
        $school->save();

        return back()->with('error_message','School Unpublished');
    }
    //published School 

    public function publishedSchool($id){

        $school = School::find($id); 
        $school->status = 1;
        $school->save(); 
        return back()->with('message','School published');
    }
    //Edit School 
    public function editSchool($id){
        $school = School::find($id);
        return view('admin.settings.school.edit',compact('school'));
    }

    //Update School 

    public function updateSchool(Request $request){
        $school = School::find($request->school_id);
        $school->school_name = $request->school_name;
        $school->save(); 
        return redirect('/school/manage')->with('message','School Updated');
    }

    //Delete School

    public function deleteSchool($id){
        $school = School::find($id);

        $school->delete();
        return redirect('/school/manage')->with('error_message','School Deleted');
    }
}
