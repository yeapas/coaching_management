<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function showLoginForm()
    { 
        $users = User::all();
        if(count($users) > 0){
            return view('admin.users.login-form');
        }else{
            $user = new User(); 
            $user->role = 'Admin';
            $user->name = 'Yeapes';
            $user->mobile = '8801867734016';
            $user->email = 'yeapesh2831@gmail.com';
            $user->password = Hash::make('password');
            $user->save();
            return view('admin.users.login-form');
        }
        // return view('auth.login');
    }

    public function username()
    {
        // return 'email';
        return 'mobile';
    }

    protected function loggedOut(Request $request)
    {
        //
        return redirect('/home');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
