<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassName;
use App\Batch;

class BatchManagementController extends Controller
{
    //
    //Add Batch
    public function addBatch(){
        $classes = ClassName::all();
        return view('admin.settings.batch.add',compact('classes'));
    }
    //Save Batch
    public function saveBatch(Request $request){
        $this->validate($request,[
            'class_id' => 'required',
            'batch_name' => 'required|string',
            'student_capacity' => 'required|string',
        ]);
        $batch = new Batch();
        $batch->class_id = $request->class_id;
        $batch->batch_name = $request->batch_name;
        $batch->student_capacity = $request->student_capacity;
        $batch->status = 1; 
        $batch->save();

        return back()->with('message','Batch Added Succesfully');
    }

    //Manage Batch
    public function manageBatch(){
        $classes = ClassName::all();
        return view('admin.settings.batch.manage',compact('classes'));
    }

    //Batch List By Ajax
    public function batchListByAjax(Request $request){
        $batches = Batch::where('class_id', $request->id)->get();

        if(count($batches)>0){
            return view('admin.settings.batch.batch-list-by-ajax',compact('batches'));
        }else{
            return view('admin.settings.batch.error');
        }
    }

    public function batchUnpublished(Request $request){
        $batch = Batch::find($request->batch_id);

        $batch->status = 2;
        $batch->save();
        
        $batches = Batch::where('class_id', $request->class_id)->get();
        return view('admin.settings.batch.batch-list-by-ajax',compact('batches'));

    }
    public function batchPublished(Request $request){
        $batch = Batch::find($request->batch_id);

        $batch->status = 1;
        $batch->save();

        $batches = Batch::where('class_id', $request->class_id)->get();
        return view('admin.settings.batch.batch-list-by-ajax',compact('batches'));

    }
    public function batchDelete(Request $request){
        $batch = Batch::find($request->batch_id);

        $batch->delete();

        $batches = Batch::where('class_id', $request->class_id)->get();
        if(count($batches)>0){
            return view('admin.settings.batch.batch-list-by-ajax',compact('batches'));
        }else{
            return view('admin.settings.batch.error');
        }
        
    }

    public function batchEdit($id){
        $batch = Batch::find($id); 
        $classes = ClassName::all();
        return view('admin.settings.batch.edit',compact('classes','batch'));
    }


    public function updateBatch(Request $request){
        $this->validate($request,[
            'class_id' => 'required',
            'batch_name' => 'required|string',
            'student_capacity' => 'required'
        ]);
        $batch = Batch::find($request->batch_id);
        $batch->class_id = $request->class_id;
        $batch->batch_name = $request->batch_name;
        $batch->student_capacity = $request->student_capacity;
        $batch->save();

        return redirect('/batch/manage')->with('message','Batch Updated Successful'); 
    }
}
