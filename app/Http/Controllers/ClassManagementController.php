<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassName;

class ClassManagementController extends Controller
{
    //
    // 
    public function addClass(){

        return view('admin.settings.class.add');
    }

    public function saveClass(Request $request){
        $this->validate($request,[
            'class_name' => 'required|string',
        ]);
        $class = new ClassName();
        $class->class_name = $request->class_name;
        $class->status = 1;
        $class->save();

        return back()->with('message','Class Added');
    }

    //class Manage Or class List

    public function manageClass(){
        $classes = ClassName::all();
        return view('admin.settings.class.manage',compact('classes'));
    }

    // Unpublished Class 

    public function unpublishedClass($id){
        $class = ClassName::find($id);
        $class->status = 2;
        $class->save();

        return back()->with('error_message','class Unpublished');
    }

    // //published School 

    public function publishedClass($id){

        $class = ClassName::find($id); 
        $class->status = 1;
        $class->save(); 
        return back()->with('message','class published');
    }

    //Edit Classs 
    public function editClass($id){
        $class = ClassName::find($id);
        return view('admin.settings.class.edit',compact('class'));
    }

    // //Update Class 

    public function updateClass(Request $request){
        $this->validate($request,[
            'class_name' => 'required|string',
        ]);

        $class = ClassName::find($request->class_id);
        $class->class_name = $request->class_name;
        $class->save(); 
        return redirect('/class/manage')->with('message','Class Updated');
    }

    // //Delete Class

    public function deleteClass($id){
        $class = ClassName::find($id);

        $class->delete();
        return redirect('/class/manage')->with('error_message','Class Deleted');
    }
}
