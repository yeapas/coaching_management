<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use Image;
class SlideController extends Controller
{
    //
    public function addSlide(){
        return view('admin.slide.add-slider');
    }

    public function uploadSlider(Request $request){
        $this->validate($request,[
            'slide_image' => 'required',
            'slide_title' => 'required',
            'slide_description' => 'required',
            'status' => 'required'
        ]);

        $slideData = new Slider();
        $slideData->slide_image = $this->createSlide($request);
        $slideData->slide_title = $request->slide_title;
        $slideData->slide_description = $request->slide_description;
        $slideData->status = $request->status;
        $slideData->save();

        return back()->with('message','Slider Uploded Successfull');
    }

    protected function createSlide($request){
        $file = $request->file('slide_image');
        $image = $file->getClientOriginalName();
        $path = 'admin/assets/slider/';
        $imageUrl = $path.$image;
        Image::make($file)->resize(1400,570)->save($imageUrl);
        return $imageUrl;
    }
    public function manageSlider(){
        $sliders = Slider::all();

        return view('admin.slide.manage-slider',compact('sliders'));
    } 
    public function unpublishSlide($id){
        $slider = Slider::find($id);
        $slider->status = 2;
        $slider->save();
        return back()->with('error_message','Slide Unpublished');
    }
    public function publishSlide($id){
        $slider = Slider::find($id);
        $slider->status = 1;
        $slider->save();
        return back()->with('message','Slide published');
    }
    public function gallery(){
        $gallery = Slider::all();
        // return $gallery; 
        return view('admin.slide.gallery',compact('gallery'));
    }

    public function slideEdit($id){
        $slider = Slider::find($id);

        return view('admin.slide.slide-edit',compact('slider'));
    }
    public function updateSlider(Request $request){
        $this->validate($request,[ 
            'slide_title' => 'required',
            'slide_description' => 'required',
            'status' => 'required'
        ]);
        $slider = Slider::find($request->slide_id);
        // return $slider;
        
        $slider->slide_title = $request->slide_title;
        $slider->slide_description = $request->slide_description;
        $slider->status = $request->status;
        if($request->file('slide_image')){
            unlink($slider->slide_image);
            $slider->slide_image = $this->createSlide($request);
        } 
        $slider->save();
        return redirect('/manage-slider')->with('message','Slider Updated Successfull');
    }

    public function slideDelete($id){
        $slide = Slider::find($id);

        if(unlink($slide->slide_image)){
            $slide->delete();
            return back()->with('error_message','Slide Deleted');
        }
       
    }
}
