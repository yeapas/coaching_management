<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HeaderFooter;
use DB;
class HomePageController extends Controller
{
    //
    public function addHeaderFooter(){  
        $headerFooter = DB::table('header_footers')->first();
        if (isset($headerFooter)) {
            return view('admin.home.manage-header-footer',compact('headerFooter'));
        }else{
            return view('admin.home.add-header-footer');
        }
    }
    public function headerFooterSave(Request $request){
    $this->validateHeaderFooter($request);
    $data = new HeaderFooter(); 
    $data->title_name = $request->title_name;
    $data->subtitle_name = $request->subtitle_name;
    $data->address = $request->address;
    $data->mobile = $request->mobile;
    $data->copyright = $request->copyright;
    $data->status = $request->status;
    $data->save();
    return redirect('/home')->with('message','Header & Footer Succesfully Added');     
    }

    public function manageHeaderFooter($id){
        $headerFooter = HeaderFooter::find($id); 
        
        return view('admin.home.manage-header-footer',compact('headerFooter'));
    }

    public function updateManageHeaderFooter(Request $request){
        $this->validateHeaderFooter($request);
        $headerFooter = HeaderFooter::find($request->id);
        $headerFooter->title_name = $request->title_name;
        $headerFooter->subtitle_name = $request->subtitle_name;
        $headerFooter->address = $request->address;
        $headerFooter->mobile = $request->mobile;
        $headerFooter->copyright = $request->copyright; 
        $headerFooter->save(); 
        return redirect('/home')->with('message','Header & Footer Succesfully Updated');
    }

    public function validateHeaderFooter($request){
        $this->validate($request,[
            'title_name' => 'required',
            'subtitle_name' => 'required',
            'address' => 'required',
            'mobile' => 'required',
            'copyright' => 'required'
        ]);
    }
}
