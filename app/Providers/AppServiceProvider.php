<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use Auth;
use App\HeaderFooter;
use DB;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        View::composer('admin.partials.nav', function ($view) { 
            $user = Auth::user();
            $header = DB::table('header_footers')->first();
            $view->with(['user' => $user, 'header' => $header]);
        });
        View::composer('admin.partials.header',function($view){
            $header = DB::table('header_footers')->first();
            $view->with('header',$header);
        });

        View::composer('admin.partials.footer',function($view){
            $footer = DB::table('header_footers')->first();
            $view->with('footer',$footer);
        });
    }
}
