@extends('admin.master')
@section('content')
    <!--Content Start-->
<section class="container-fluid">
    <div class="row content registration-form">
        <div class="col-12 pl-0 pr-0">
            <div class="form-group">
                <div class="col-sm-12">
                    <h4 class="text-center font-weight-bold font-italic mt-3">User Registration Form</h4>
                </div>
            </div>
            <form method="POST" action="{{ route('header-footer-save') }}" enctype="multipart/form-data" autocomplete="off" class="form-inline">
                @csrf
                  
                <div class="form-group col-12 mb-3">
                    <label for="title_name" class="col-sm-3 col-form-label text-right">Title Name</label>
                    <input id="title_name" type="text" class="col-sm-9 form-control @error('title_name') is-invalid @enderror" name="title_name" placeholder="Title Name" value="{{ old('title_name') }}" required autocomplete="title_name" autofocus>
                    @error('title_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group col-12 mb-3">
                    <label for="title_name" class="col-sm-3 col-form-label text-right">Subtitle Name</label>
                    <input id="subtitle_name" type="text" class="col-sm-9 form-control @error('subtitle_name') is-invalid @enderror" name="subtitle_name" placeholder="Subtitle Name" value="{{ old('subtitle_name') }}" required autocomplete="subtitle_name" autofocus>
                    @error('subtitle_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-12 mb-3">
                    <label for="address" class="col-sm-3 col-form-label text-right">Address</label>
                    <input id="address" type="text" class="col-sm-9 form-control @error('address') is-invalid @enderror" name="address" placeholder="Address" value="{{ old('address') }}" autocomplete="address" required>
                    @error('address')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div> 
                <div class="form-group col-12 mb-3">
                    <label for="mobile" class="col-sm-3 col-form-label text-right">Mobile</label>
                    <input id="mobile" type="text" class="col-sm-9 form-control" name="mobile" value="{{ old('mobile') }}" placeholder="8801xxxxxxxxx" autocomplete="mobile" required>
                </div>
                <div class="form-group col-12 mb-3">
                    <label for="copyright" class="col-sm-3 col-form-label text-right">Copyright</label>
                    <input id="copyright" type="text" class="col-sm-9 form-control @error('copyright') is-invalid @enderror" name="copyright" placeholder="Copyright" value="{{ old('copyright') }}" autocomplete="copyright" required>
                    @error('copyright')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div> 
                <input type="hidden" name="status" value="1">
                <div class="form-group col-12 mb-3">
                    <label class="col-sm-3"></label>
                    <button type="submit" class="col-sm-9 btn btn-block my-btn-submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>
<!--Content End-->
@endsection
