@extends('admin.master')

@section('content')
@if (Session::get('error_message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong class="text-danger">Message: {{  Session::get('error_message')  }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>      
@endif
@if (Session::get('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Message: </strong>{{  Session::get('message')  }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>      
@endif
    <!--Content Start-->
<section class="container-fluid">
    <div class="row content">
        <div class="col-12 pl-0 pr-0">
            <div class="form-group">
                <div class="col-sm-12">
                    <h4 class="text-center font-weight-bold font-italic mt-3">School List</h4>
                </div>
            </div>

            <div class="table-responsive p-1">
                <table id="example" class="table table-striped table-bordered dt-responsive nowrap text-center" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Name</th> 
                        <th>Status</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($schools as $school)
                        <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $school->school_name }}</td>
                        <td>{{ $school->status == 1 ? 'Published': 'Unpublished' }}</td> 
                            <td>
                                @if ($school->status == 1)
                                    <a href="{{ route('unpublished-school', ['id' => $school->id]) }}" class="btn btn-sm btn-success" title="Unpublished"><span class="fa fa-arrow-alt-circle-up"></span></a> 
                                @else
                                <a href="{{ route('published-school', ['id' => $school->id]) }}" class="btn btn-sm btn-warning" title="Published"><span class="fa fa-arrow-alt-circle-down"></span></a>
                                @endif 
                            <a href="{{ route('edit-school', ['id' => $school->id]) }}" class="btn btn-sm btn-info" title="Edit"><span class="fa fa-edit"></span></a>

                                <a href="{{ route('delete-school', ['id' => $school->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Are Sure To Delete ?')"  title="Delete"><span class="fa fa-trash"></span></a>
                            </td>
                        </tr> 
                        @endforeach 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!--Content End-->
@endsection