<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="studenTypeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Student Type</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="{{ route('save-student-type') }}" method="POST" id="studentTypeForm">
                @csrf
            <div class="form-group row mb-0">
                <label for="classId" class="col-form-label col-sm-3 text-right">Class Name</label>
                <div class="col-md-9">
                   <select name="class_id" class="custom-select custom-select-lg mb-3 @error('class_id') is-invalid @enderror" id="classId" required autofocus>
                       <option value="">Select Class</option>
                       @foreach ($classes as $class)
                        <option value="{{ $class->id }}">{{ $class->class_name }}</option>
                       @endforeach
                   </select>
                    @error('class_id')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="classId" class="col-form-label col-sm-3 text-right">Student Type Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control @error('student_type') is-invalid @enderror" name="student_type" id="studentType" required autofocus autocomplete="">
                    @error('student_type')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
            </div>
        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" id="reset">Reset</button>
          <button type="submit" class="btn btn-success">Save</button>
        </div>
    </form>
      </div>
    </div>
  </div>
  <!-- Modal X -->