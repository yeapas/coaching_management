<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Student Type</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="{{ route('update-student-type') }}" method="POST" id="studentEditForm">
              @csrf
          <div class="form-group row mb-0">
              <label for="classId" class="col-form-label col-sm-3 text-right">Student Type Name</label>
              <div class="col-md-9">
                  <input type="text" class="form-control @error('student_type') is-invalid @enderror" name="student_type" id="studentTypeEdit" required autofocus >
                  @error('student_type')
                  <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                  @enderror
              </div>
          </div>
          <input type="hidden" name="type_id" id="typeId">
      </div>
      <div class="modal-footer">
        <button type="button" class="d-none">Reset</button>
        <button type="submit" class="btn btn-success">Update</button>
      </div>
   </form>
    </div>
  </div>
</div>
<!-- Modal X -->