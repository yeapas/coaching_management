@extends('admin.master')

@section('content')

    <!--Content Start-->
<section class="container-fluid">
    <div class="row content">
        <div class="col-12 pl-0 pr-0">
            <div class="form-group">
                <div class="col-sm-12">
                    <h4 class="text-center font-weight-bold font-italic mt-3">Student Type List <button class="btn-success text-light" id="studentTypeAdd" data-toggle="modal" data-target="#studenTypeModal">Add New</button></h4> 
                </div>
            </div>

            <div class="table-responsive p-1">
                <table id="example" class="table table-striped table-bordered dt-responsive nowrap text-center" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Class Name</th> 
                        <th>Student Type</th> 
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody id="studentTypeList">
                        @include('admin.settings.student-type.ajax-list')
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('admin.settings.student-type.modal.add-form')
    @include('admin.settings.student-type.modal.edit-form')
  
  
</section>
<!--Content End-->
<script>

    //Add Student Type
    $('#studentTypeForm').submit(function(e){
        e.preventDefault();
        let url = $(this).attr('action');
        let method = $(this).attr('method');
        let data = $(this).serialize();
        // $('#studenTypeModal #reset').click();
        $('#studenTypeModal').modal('hide'); 
        $.ajax({
            type: method,
            url: url,
            data: data,
            success: function(){
                $.get("{{route('student-type-list')}}",function(data){ 
                    $('#studentTypeList').empty().html(data); 
                });
                // $("#studentTypeList")[0].reset();
            }
        }); 
    });

    //Student Type Unpublish
    function studentTypeUnpublish(id){
       $.get("{{route('student-type-unpublish')}}",{type_id:id},function(data){
            $('#studentTypeList').empty().html(data);
       });
    }
    //Student Type Publish
    function studentTypePublish(id){
       $.get("{{route('student-type-publish')}}",{type_id:id},function(data){
            $('#studentTypeList').empty().html(data);
       });
    }

    //Student Edit 
    function studentTypeEdit(id,student_type){
        $('#editModal').find('#studentTypeEdit').val(student_type);
        $('#editModal').find('#typeId').val(id);
        $('#editModal').modal('show'); 
    }


    $('#studentEditForm').submit(function(e){
        e.preventDefault();
        let url = $(this).attr('action');
        let method = $(this).attr('method');
        let data = $(this).serialize();
        // $('#studenTypeModal #reset').click();
        $('#editModal').modal('hide'); 
        $.ajax({
            type: method,
            url: url,
            data: data,
            success: function(data){
                $('#studentTypeList').empty().html(data);
                // $("#studentTypeList")[0].reset();
            }
        }); 
    });
    
    function studentTypeDelete(id){
        let msg = "Do You want to delete ?"; 
        if(confirm(msg)){
            $.get("{{route('student-type-delete')}}",{type_id:id},function(data){
            $('#studentTypeList').empty().html(data);
            console.log(id)
       });
        }
    }
</script>
@endsection