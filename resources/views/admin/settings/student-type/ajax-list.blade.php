@if(count($studentTypes)>0)
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($studentTypes as $studentType)
                        <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $studentType->class_name }}</td>
                        <td>{{ $studentType->student_type }}</td>
                        <td>{{ $studentType->status == 1 ? 'Active': 'Inactive' }}</td> 
                            <td>
                                @if ($studentType->status == 1)
                            <button onclick="studentTypeUnpublish({{$studentType->id}});" class="btn btn-sm btn-success" title="Unpublished"><span class="fa fa-arrow-alt-circle-up"></span></button> 
                                @else
                                <button onclick="studentTypePublish({{$studentType->id}});" class="btn btn-sm btn-warning" title="Published"><span class="fa fa-arrow-alt-circle-down"></span></button>
                                @endif 
                            <button  onclick="studentTypeEdit({{$studentType->id}},'{{ $studentType->student_type }}');"  class="btn btn-sm btn-info fa fa-edit" title="Edit"></button>

                            <button onclick="studentTypeDelete('{{$studentType->id}}')" class="btn btn-sm btn-danger"  title="Delete"><span class="fa fa-trash"></span></button>
                            </td>
                        </tr> 
                        @endforeach
                        @else 
                        <tr>
                            <td colspan="5" class="text-danger">Student Type Not Found</td>
                        </tr>
                        @endif 

                        