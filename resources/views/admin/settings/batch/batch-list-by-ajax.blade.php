<thead>
    <tr>
        <th>Sl.</th>
        <th>Name</th> 
        <th>Capacity</th> 
        <th>Status</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
        @endphp
        @foreach ($batches as $batch)
        <tr>
        <td>{{ $i++ }}</td>
        <td>{{ $batch->batch_name }}</td>
        <td>{{ $batch->student_capacity }}</td>
        <td>{{ $batch->status == 1 ? 'Published': 'Unpublished' }}</td> 
            <td>
                @if ($batch->status == 1)
                    <button onclick="unpublished({{ $batch->id }},{{ $batch->class_id }})" class="btn btn-sm btn-success" title="Unpublished"><span class="fa fa-arrow-alt-circle-up"></span></button> 
                @else
                <button  onclick="published({{ $batch->id }},{{ $batch->class_id }})"  class="btn btn-sm btn-warning fa fa-arrow-alt-circle-down" title="Published"></button>
                @endif 

                <a href="{{ route('edit-batch', ['id' => $batch->id]) }}" class="btn btn-sm btn-info fa fa-edit" title="Edit" target="_blank"></a>

                <button href=""  onclick="deleteBatch({{ $batch->id }},{{ $batch->class_id }})" class="btn btn-sm btn-danger" onclick="return confirm('Are Sure To Delete ?')"  title="Delete"><span class="fa fa-trash"></span></button>
            </td>
        </tr> 
        @endforeach 
    </tbody>
    <script>
        function unpublished(batchId, classId){
            let check = confirm('do you want To unpublish ?');
            if(check){
                $.get("{{ route('batch-unpublished') }}",{batch_id: batchId,class_id: classId },function(data){
                    $('#batchList').empty().html(data);
                })
            }

        }
        function published(batchId, classId){
            let check = confirm('do you want To publish ?');
            if(check){
                $.get("{{ route('batch-published') }}",{batch_id: batchId,class_id: classId },function(data){
                    $('#batchList').empty().html(data);
                })
            }

        }
        function deleteBatch(batchId, classId){
            let check = confirm('do you want To Delete ?');
            if(check){
                $.get("{{ route('batch-delete') }}",{batch_id: batchId,class_id: classId },function(data){
                    $('#batchList').empty().html(data);
                })
            }

        }
         
    </script>