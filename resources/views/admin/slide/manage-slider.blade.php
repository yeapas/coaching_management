@extends('admin.master')
@section('content')
<!--Content Start-->
@if (Session::get('error_message'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong class="text-danger">Message: {{  Session::get('error_message')  }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>      
@endif
@if (Session::get('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Message: </strong>{{  Session::get('message')  }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>      
@endif
<section class="container-fluid">
    <div class="row content">
        <div class="col-12 pl-0 pr-0">
            <div class="form-group">
                <div class="col-sm-12">
                    <h4 class="text-center font-weight-bold font-italic mt-3">Manage Sliders: </h4>
                </div>
            </div>

            <div class="table-responsive p-1">
                <table id="example" class="table table-striped table-bordered dt-responsive nowrap text-center" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Slide Title</th>
                        <th>Slide Description</th>
                        <th>Slide Image</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($sliders as $slider)
                            <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $slider->slide_title }}</td>
                            <td>{{ $slider->slide_description }}</td>
                            <td><img src="{{ asset('/').$slider->slide_image }}" width="200" height="100" alt="Slide Image"></td>
                            <td>
                                @if ($slider->status == 1)
                            <a href="{{ route('unpublish-slide',['id' => $slider->id]) }}" class="btn btn-success fa fa-arrow-alt-circle-up" title="Click To Deactive"><i class="fa fa-arrows" aria-hidden="true"></i></a>
                                @else 
                            <a href="{{ route('publish-slide',['id' => $slider->id]) }}" class="btn btn-warning fa fa-arrow-alt-circle-down" title="Click to Active"></a>
                                @endif
                            <a href="{{ route('slide-edit',['id' => $slider->id]) }}" class="btn btn-info fa fa-edit"></a>
                            <a href="{{ route('slide-delete', ['id' => $slider->id]) }}" class="btn btn-danger fa fa-trash-alt" onclick="return confirm('Do You Want to Delete ?')"></a>
                            </td>
                            </tr> 
                        @endforeach
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!--Content End-->
    
@endsection